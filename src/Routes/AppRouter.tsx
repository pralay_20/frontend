// AppRouter.tsx
import React from 'react';
import { BrowserRouter as Router, Route, Routes, Navigate} from 'react-router-dom';
import FormComponent from '../Pages/FormComponent';

const AppRouter: React.FC = () => {
    return (
        <Router basename="/form-app/">
            <Routes>
                <Route path="/formfill/:id" element={<FormComponent/>}/>
                <Route path="/home" element = {<Navigate to = "https://www.google.com" replace />}/>
            </Routes>
        </Router>
    );
};

export default AppRouter;
