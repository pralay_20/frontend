import React from 'react';
import './App.css';
import AppRouter from './Routes/AppRouter';
import 'bootstrap/dist/css/bootstrap.css';

function App() {
  return (
    <div className="App">
      <AppRouter/>
    </div>
  );
}

export default App;
