import axios from 'axios'
import { Constants } from '../utilities/constants';

export const fetchData = async (id: string | undefined) => {
    try {
        console.log("ID", id)
        console.log(process.env)
        console.log("dsd", Constants.URLS.FORM_BACKEND_BASE_PUB_URL);
        let response = await axios.get(`${Constants.URLS.FORM_BACKEND_BASE_PUB_URL}/form/${id}`)
        return response.data;
    } catch (error) {
        console.log("Error FetchData", error);
    }
}


export const submitForm = async (payload: object, formId: string | undefined) => {
    try {
        const response = await axios.post(`${Constants.URLS.FORM_BACKEND_BASE_PUB_URL}/form/submit/${formId}`, payload)
        return response.data;
        //console.log("Post request succesfull",response.data)
    } catch (error) {
        console.error("@submitForm:", JSON.stringify(error))
    }
}


