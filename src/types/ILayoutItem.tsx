export interface LayoutItem {
    config: {
        style: string;
        direction?: FlexDirection;
        column?: string;
        row?: string;
    };
    items: Array<{
        'flex-grow'?: string;
        colspan?: string;
        rowspan?: string;
        id: string;
    }>;
}

type FlexDirection = 'row' | 'row-reverse' | 'column' | 'column-reverse';
