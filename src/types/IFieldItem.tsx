export interface FieldItem {
    id: string;
    type: string;
    text?: string;
    readonly?: boolean;
    disabled?:boolean;
    defaultvalue?: string;
    values?: string[];
    placeholder?: string;
    name?:string;
    required?: boolean;
    inputType?: string;
};