export const config = [
    {
        type: "textfield",
        placeholder: "name",
        defaultvalue: ""
    },
    {
        type: "textfield",
        placeholder: "address",
        defaultvalue: ""
    },
    {
        type: "label",
        text: "hello"
    },
    {
        type: "button",
        text: "submit"
    }
]



export const config2 = {
    "field": [{
        "id": "1",
        "type": "label",
        "text": "Gender"
    },
    {
        "id": "2",
        "type": "radio",
        "text": "gender",
        "editable": true,
        "defaultvalue": "male",
        "values": ["male", "female"]
    },
    {
        "id": "3",
        "type": "label",
        "text": "Personal Details"
    },
    {
        "id": "4",
        "type": "textfield",
        "placeholder": "name",
        "defaultvalue": "",
        "editable": true,
        "required": true,
        "inputType": "alphabet"
    },
    {
        "id": "5",
        "type": "button",
        "editable": true,
        "text": "test"
    },
    {
        "id": "6",
        "type": "button",
        "editable": true,
        "text": "test"
    },
    {
        "id": "7",
        "type": "button",
        "editable": true,
        "text": "test"
    }, {
        "id": "8",
        "type": "button",
        "editable": true,
        "text": "test"
    }, {
        "id": "9",
        "type": "submit",
        "editable": true,
        "text": "submit"
    }
    ],
    "layout": [{
        "config": {
            "style": "flex",
            "direction": "column"
        },
        "items": [{
            "flex-grow": "3",
            "id": "1"
        }, {
            "flex-grow": "3",
            "id": "2"
        }]
    },
    {
        "config": {
            "style": "flex",
            "direction": "column"
        },
        "items": [{
            "flex-grow": "2",
            "id": "3"
        }, {
            "flex-grow": "2",
            "id": "4"
        }]
    },
    {
        "config": {
            "style": "grid",
            "column": "2",
            "row": "2"
        },
        "items": [{
            "colspan": "2",
            "rowspan": "2",
            "id": "5"
        },
        {
            "colspan": "2",
            "rowspan": "2",
            "id": "6"
        },
        {
            "colspan": "2",
            "rowspan": "2",
            "id": "7"
        },
        {
            "colspan": "2",
            "rowspan": "2",
            "id": "8"
        }
        ]
    },
    {
        "config": {
            "style": "flex",
            "direction": "row"
        },
        "items": [{
            "flex-grow": "2",
            "id": "9"
        }]
    }
    ]

}