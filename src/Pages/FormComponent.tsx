import React, { FC, useEffect, useState } from 'react'
import { FieldItem } from '../types/IFieldItem'
import { LayoutItem } from '../types/ILayoutItem'
import Label from '../components/LabelComponent'
import Button from '../components/ButtonComponent'
import Textbox from '../components/TextboxComponent'
import Form from 'react-bootstrap/Form';
import { fetchData, submitForm } from '../services/axios'
import { useParams, useNavigate, useLocation } from 'react-router-dom'
import RadioComponent from '../components/RadioComponent/RadioComponent'

interface FormData {
    field: FieldItem[],
    layout: LayoutItem[]
}


const FormComponent: FC = () => {

    const { id } = useParams<{ id: string }>();
    const location = useLocation();
    const queryparams = new URLSearchParams(location.search);
    const redirect_url = queryparams.get('redirect_url');
    const [config, setConfig] = useState<FormData>();
    const [formValues, setFormValues] = useState<{ [key: string]: string }>({})

    useEffect(() => {
        const fetch = async () => {
            let response = await fetchData(id);
            console.log(response.data);
            setConfig(response.data);
        }
        fetch()

    }, [])

    const handleTextChange = (fieldName: string, newValue: string) => {
        setFormValues((prev) => ({
            ...prev,
            [fieldName]: newValue
        }))
    }

    const handleRadioChange = (radioLabel: string, newValue: string) => {
        setFormValues((prev) => {
            return ({
                ...prev,
                [radioLabel]: newValue
            })
        })
    }

    const handleSubmit = async (e: React.FormEvent) => {
        e.preventDefault();
        console.log("formSubmitted", formValues)
        await submitForm(formValues, id)
        const URL = redirect_url + `?status=SUBMITTED`;
        window.location.href = URL
    }


    const renderField = (field: FieldItem) => {
        switch (field.type) {
            case 'label':
                return <Label text={field.text || ""} />
            case 'textfield':
                return (
                    <Textbox config={field} onChange={handleTextChange} />
                );
            case 'radio':
                return (
                    <RadioComponent config={field} onChange={handleRadioChange} />
                )
            case 'button':
                return (
                    <Button config={field} />

                );
            case 'submit':
                return (
                    <Button config={field} />
                );
            default:
                return null;
        }
    }

    const renderLayout = (layout: LayoutItem) => {
        const { style, direction, row, column } = layout.config;
        const layoutStyle: React.CSSProperties = {
            display: style === 'grid' ? 'grid' : 'flex',
            flexDirection: direction || 'row',
            gridTemplateRows: `repeat(${row}, 1fr)`,
            gridTemplateColumns: `repeat(${column}, 1fr)`,
        }

        return (
            <div style={layoutStyle}>
                {layout.items.map((item) => (
                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1"
                        key={item.id}
                        style={{
                            flexGrow: item['flex-grow'],
                            gridColumn: `${item.colspan} / span 1`,
                            gridRow: `${item.rowspan} / span 1`,
                        }}
                    >
                        {renderField(config?.field?.find((field) => field.id === item.id)!)}
                    </Form.Group>
                ))}
            </div>
        )
    }


    return (
        <Form onSubmit={handleSubmit}>
            {config ?
                config.layout.map((layout, index) => (
                    <div key={index}>{renderLayout(layout)}</div>
                )) : <p>loading</p>}
        </Form>
    )
}

export default FormComponent