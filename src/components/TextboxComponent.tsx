import React, { useState } from 'react'
import Form from 'react-bootstrap/Form';
import { FieldItem } from '../types/IFieldItem';

interface TextBoxProps {
    config: FieldItem
    onChange: (fieldName: string, newValue: string) => void;
}

const TextboxComponent: React.FC<TextBoxProps> = ({ config, onChange }) => {

    const [textValue, setTextValue] = useState<string>(config.defaultvalue || "")

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setTextValue(e.target.value)
        onChange(e.target.name, e.target.value)
    }

    return (
        <Form.Control
            type="text"
            value={textValue}
            readOnly={config.readonly}
            placeholder={config.placeholder}
            name={config.name}
            onChange={handleChange}
        />
    )
}

export default TextboxComponent