import React from 'react'
import Form from 'react-bootstrap/Form';

interface LabelProps {
    text: string
}

const LabelComponent: React.FC<LabelProps> = ({ text }) => {
    return (
        <Form.Label>{text}</Form.Label>
    )
}

export default LabelComponent