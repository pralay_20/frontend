import React, { FC } from 'react'
import Button from 'react-bootstrap/Button';
import { FieldItem } from '../types/IFieldItem';


interface ButtonProps {
    config: FieldItem
}

const ButtonComponent: FC<ButtonProps> = ({ config }) => {
    return (
        <Button variant="primary"
            size="sm"
            type='submit'
            disabled={config.disabled}>
            {config.text}
        </Button>
    )
}

export default ButtonComponent