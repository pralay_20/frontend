import React, { FC, useState } from 'react'
import { FieldItem } from '../../types/IFieldItem'
import './RadioComponent.css'

interface RadioProps {
    config: FieldItem
    onChange: (radioLabel: string, newValue: string) => void
}

const RadioComponent: FC<RadioProps> = ({ config, onChange }) => {

    const [selectedValue, setSelectedValue] = useState<string>(config.defaultvalue || "");

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSelectedValue(event.target.value);
        onChange(event.target.name, event.target.value);
    };

    return (
        <div>
            {config.values?.map((value) => {
                return (
                    <div className='form-check' key={value}>
                        <input className="form-check-input"
                            type="radio"
                            name={config.name}
                            id={value}
                            checked={selectedValue === value}
                            value={value}
                            onChange={handleChange}
                            disabled={config.disabled}
                        />
                        <label className="form-check-label" htmlFor={value}>
                            {value}
                        </label>
                    </div>
                )
            })}
        </div>
    )
}

export default RadioComponent