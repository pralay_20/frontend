FROM node:18.16.0 AS production

WORKDIR /app/form-frontend
COPY package.json package-lock.json ./
RUN npm ci
COPY . .

RUN npm run build

ENV REACT_APP_FORM_BACKEND_BASE_PUB_URL="http://192.168.11.89:3000"
ENV PORT=5003
EXPOSE ${PORT}

CMD ["npm","run","start"]